#!/bin/sh

set -eu -o pipefail

WATCH_PATH="${WATCH_PATH:-"/tmp"}"
INOTIFYWAIT_OPTS="${INOTIFYWAIT_OPTS:-"-e create -e modify -e delete -e delete_self"}"
ACTION="${ACTION:-"true"}"
RUN_ACTION_ON_START="${RUN_ACTION_ON_START:-false}"

main() {
  if [[ "$RUN_ACTION_ON_START" = true ]]; then
    echo "Running action at start..."
    sh -c "$ACTION"
  fi

  while true; do
    while [[ ! -f "$WATCH_PATH" && ! -d "$WATCH_PATH" ]]; do
      echo "Waiting for $WATCH_PATH to appear ..."
      sleep 1
    done

    echo "Waiting for '$WATCH_PATH' events..."
    # shellcheck disable=SC2086
    if ! inotifywait $INOTIFYWAIT_OPTS "$WATCH_PATH"; then
      echo "WARNING: inotifywait exited with code $?"
    fi
    # small grace period before sending SIGHUP:
    sleep 1

    echo "Running action..."
    if ! sh -c "$ACTION"; then
      echo "WARNING: action exited with code $?"
    fi
  done
}
main "$@"

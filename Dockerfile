FROM alpine

ENV WATCH_PATH="/tmp"
ENV ACTION="true"
ENV INOTIFYWAIT_OPTIONS="-e modify -e delete -e delete_self"

RUN apk add inotify-tools rsync

ADD watcher.sh /usr/bin/
RUN chmod +x /usr/bin/watcher.sh

CMD ["/usr/bin/watcher.sh"]
